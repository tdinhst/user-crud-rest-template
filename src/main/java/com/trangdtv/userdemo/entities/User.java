package com.trangdtv.userdemo.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.trangdtv.userdemo.dto.UserDto;

@Entity
@Table(name = "users")
public class User {
	@Id
	private long id;
	
	@Column
	@NotBlank(message = "Name must be not blank.")
	private String name;
	
	@Column 
	@NotBlank
	@Pattern(regexp=".+@.+\\..+", message="Please provide a valid email address")
	private String email;
	
	@Column
	private String gender;
	
	@Column
	private StatusUser status;
	
	@Column
	private Date createDate;
	
	@Column
	private Date updateDate;

	public long getId() {
		return id;
	}
	@JsonProperty(access = Access.READ_ONLY)
	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public StatusUser getStatus() {
		return status;
	}

	public void setStatus(StatusUser status) {
		this.status = status;
	}

	public Date getCreateDate() {
		return createDate;
	}
	@JsonIgnore(true)
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}
	@JsonIgnore(true)
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}


	public User(@NotBlank String name,
			String email, String gender,
			StatusUser status, Date createDate, Date updateDate) {
		super();	
		this.name = name;
		this.email = email;
		this.gender = gender;
		this.status = status;
		this.createDate = createDate;
		this.updateDate = updateDate;
	}

	public User() {
		super();
	}
	
	
	public User(UserDto user) {
		super();
		this.id = user.getId();
		this.name = user.getName();
		this.email = user.getEmail();
		this.gender = user.getGender();
		this.status = StatusUser.fromValue(user.getStatus());
		this.createDate = user.getCreateDate();
		this.updateDate = user.getUpdateDate();
	}
	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", email=" + email + ", gender=" + gender + ", status=" + status
				+ ", createDate=" + createDate + ", updateDate=" + updateDate + "]";
	}
	
}
