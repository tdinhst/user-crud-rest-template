package com.trangdtv.userdemo.entities;

public enum StatusUser {
	Active("Active"),
	Inactive("Inactive");
    private final String value;

    StatusUser(String v) {
        value = v;
    }

    public String value() {
        return value;
    }
	
    public static StatusUser fromValue(String v) {
        for (StatusUser c: StatusUser.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
    
    public static boolean contains(String test) {

        for (StatusUser c : StatusUser.values()) {
            if (c.value().equals(test)) {
                return true;
            }
        }

        return false;
    }

}
