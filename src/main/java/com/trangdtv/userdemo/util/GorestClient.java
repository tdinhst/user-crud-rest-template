package com.trangdtv.userdemo.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import com.trangdtv.userdemo.dto.GoRestCreatedResponse;
import com.trangdtv.userdemo.dto.GoRestGetResponse;
import com.trangdtv.userdemo.entities.User;
import com.trangdtv.userdemo.exception.UserDemoException;

@Component
public class GorestClient {
	static final String userURL = "https://gorest.co.in/public-api/users";
	private Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	HttpUtil httpUtil;

	@Value("${gorest.token}")
	private String acessToken;

	public GoRestGetResponse getUsers() {
		GoRestGetResponse res = httpUtil.getForObject(userURL, "", this.initHeader(), GoRestGetResponse.class);
		log.info(res.getData().toString());
		log.info(res.getMeta().toString());
		return res;
	}

	public GoRestCreatedResponse createUser(User user) throws UserDemoException {
		HttpUtil httpUtil = new HttpUtil();
		GoRestCreatedResponse res = httpUtil.post(userURL, user, this.initHeaderWithToken(),
				GoRestCreatedResponse.class);
		log.info("Create user: " + res);
		return res;
	}

	public GoRestCreatedResponse updateUser(User user) throws UserDemoException {
		HttpUtil httpUtil = new HttpUtil();
		GoRestCreatedResponse res = httpUtil.put(userURL + "/" + user.getId(), user, this.initHeaderWithToken(),
				GoRestCreatedResponse.class);
		log.info("Update user: " + res);
		return res;
	}

	public GoRestCreatedResponse deleteUser(Long id) throws UserDemoException {
		HttpUtil httpUtil = new HttpUtil();
		GoRestCreatedResponse res = httpUtil.delete(userURL + "/" + id.toString(), "", this.initHeaderWithToken(),
				GoRestCreatedResponse.class);
		log.info("Delete user: " + res);
		return res;
	}

	private HttpHeaders initHeaderWithToken() {
		HttpHeaders headers = this.initHeader();
		headers.add("Authorization", "Bearer " + acessToken);
		return headers;
	}

	private HttpHeaders initHeader() {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
		headers.add("Content-Type", MediaType.APPLICATION_JSON_VALUE);
		return headers;
	}
}
