package com.trangdtv.userdemo.util;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.trangdtv.userdemo.exception.UserDemoException;


@Component
public class HttpUtil {

	RestTemplate restTemplate = new RestTemplate();

	public <T> T getForObject(String url, Object request,  HttpHeaders headers, Class<T> responseType) {
		HttpEntity<Object> httpEntity = initHttpEntity(request,headers);
		ResponseEntity<T> resen = restTemplate.exchange(url, HttpMethod.GET, httpEntity, responseType);
		return resen.getBody();
	}

	public <T> T put(String url, Object request, HttpHeaders headers,  Class<T> responseType) {
		HttpEntity<Object> httpEntity = initHttpEntity(request,headers);
		ResponseEntity<T> resen = restTemplate.exchange(url, HttpMethod.PUT, httpEntity, responseType);
		return resen.getBody();
	}

	public <T> T post(String url, Object request, HttpHeaders headers,  Class<T> responseType) throws UserDemoException {
		HttpEntity<Object> httpEntity = initHttpEntity(request,headers);
		ResponseEntity<T> resen = restTemplate.exchange(url, HttpMethod.POST, httpEntity, responseType);
		if (resen.getStatusCode() == HttpStatus.OK) {
			return resen.getBody();
		}
		return resen.getBody();
	}

	public <T> T delete(String url, Object request, HttpHeaders headers, Class<T> responseType) {
		HttpEntity<Object> httpEntity = initHttpEntity(request,headers);
		ResponseEntity<T> resen = restTemplate.exchange(url, HttpMethod.DELETE, httpEntity, responseType);
		return resen.getBody();
	}

	private HttpEntity<Object> initHttpEntity(Object request, HttpHeaders headers) {
		return new HttpEntity<>(request, headers);
	}
}