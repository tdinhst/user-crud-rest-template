package com.trangdtv.userdemo.service;

import java.util.List;

import com.trangdtv.userdemo.entities.User;
import com.trangdtv.userdemo.exception.UserDemoException;

public interface UserService {
	User create(User user) throws UserDemoException;
	User update(User user) throws UserDemoException;
	void delete (long id) throws UserDemoException;
	User findById(long id);
	List<User> findAll(String name, int pageNumber, int recordOfPage);
	Integer getUsersAndSendToKafka() throws UserDemoException;
	

}
