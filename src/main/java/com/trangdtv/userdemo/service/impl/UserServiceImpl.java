package com.trangdtv.userdemo.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.trangdtv.userdemo.dto.GoRestCreatedResponse;
import com.trangdtv.userdemo.dto.GoRestGetResponse;
import com.trangdtv.userdemo.dto.UserDto;
import com.trangdtv.userdemo.dto.UserKafka;
import com.trangdtv.userdemo.entities.User;
import com.trangdtv.userdemo.exception.UserDemoException;
import com.trangdtv.userdemo.repository.UserRepository;
import com.trangdtv.userdemo.service.UserService;
import com.trangdtv.userdemo.util.GorestClient;

@Service
public class UserServiceImpl implements UserService{
	
	@Autowired
	GorestClient goRestClient;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	@Qualifier(value = "kafkaTemplate")
	private KafkaTemplate<String, UserDto> userKafkaTemplate;
	
	@Autowired
    @Qualifier(value = "kafkaTemplateForString")
	private KafkaTemplate<String, String> kafkaTemplateForString;
	
	private Logger log = LoggerFactory.getLogger(getClass());
	
	@Value("${kafka.topic.user}")
	private String userTopic;
	
	@Value("${kafka.topic.simple}")
	private String simpleTopic;
	
	

	@Override
	public User create(User user) throws UserDemoException {	
		GoRestCreatedResponse goRestRespone= goRestClient.createUser(user);
		log.info("Submit user {0}", user);
		log.info("Response {0}", goRestRespone);
			if(goRestRespone.getCode() == 201 && goRestRespone.getUser() != null) {
				if(userRepository.countByEmail(user.getEmail()) != 0){
					throw new UserDemoException("Email is exist in DB.");
				}
				user.setId(goRestRespone.getUser().getId());
				log.info("Creating user {0}", user);
				user.setCreateDate(goRestRespone.getUser().getCreateDate());
				user.setUpdateDate(goRestRespone.getUser().getUpdateDate());
				userRepository.save(user);
			} else {
				throw new UserDemoException("Submit failed. Status code: "+ goRestRespone.getCode());
			}
		return user;
	}

	@Override
	public User update(User user) {
		GoRestCreatedResponse goRestRespone= goRestClient.updateUser(user);
		log.info("Submit user {0}", user);
		log.info("Response {0}", goRestRespone);
		if(goRestRespone.getCode() == 200 && goRestRespone.getUser() != null) {
			if(userRepository.countById(user.getId()) == 0){
				throw new UserDemoException("User not found in DB");
			}
			
			User currentUser = userRepository.findById(user.getId()).get();
			
			if(userRepository.countByEmail(user.getEmail()) != 0
					&& !currentUser.getEmail().equalsIgnoreCase(user.getEmail())){
				throw new UserDemoException("Email is exist in DB.");
			} 
			user.setUpdateDate(goRestRespone.getUser().getUpdateDate());
			userRepository.save(user);
			
		}else {
			throw new UserDemoException("Update failed. Status code: "+ goRestRespone.getCode());
		}
		
	
		return user;
		
	}

	@Override
	public void delete(long id) {
		GoRestCreatedResponse goRestRespone= goRestClient.deleteUser(id);
		log.info("Submit id {0}", id);
		log.info("Response {0}", goRestRespone);
		if(goRestRespone.getCode() == 204) {
			if(userRepository.countById(id) == 0){
				throw new UserDemoException("User not found in DB");
			} 
			userRepository.deleteById(id);
		}else {
			throw new UserDemoException("Delete failed. Status code: "+ goRestRespone.getCode());
		}
			
	}

	@Override
	public User findById(long id) {
		Optional<User> userById = userRepository.findById(id);
		if (userById.isPresent()) {
			return userById.get();
		}
		return null;
	}

	@Override
	public List<User> findAll(String name, int pageNumber, int recordOfPage) {
		log.info(goRestClient.getUsers().toString());
		if(name == null || "".equals(name)) {
		 return userRepository.findAll(PageRequest.of(pageNumber, recordOfPage)).getContent();
		}
		return userRepository.findAllByName("name", PageRequest.of(pageNumber, recordOfPage));
 
	}

	@Override
	public Integer getUsersAndSendToKafka() throws UserDemoException{
		List<UserDto> users;
		GoRestGetResponse response =  goRestClient.getUsers();
		if(response != null) {
			if(response.getCode() != HttpStatus.OK.value()) {
				throw new UserDemoException("Get data failed. Status code: "+ response.getCode());
			}
			
			users = response.getData();

			
			if(CollectionUtils.isNotEmpty(users)) {
				List<UserKafka> usersKafka = new ArrayList<>();
				for (UserDto user : users) {
					saveUser(user);
					UserKafka userKafka = new UserKafka(user.getId(), user.getName(),user.getStatus());
					usersKafka.add(userKafka);
				}
				
				ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
				try {
					String json = ow.writeValueAsString(usersKafka);

					this.postMessage(json);
				} catch (Exception e) {
					throw new UserDemoException("JsonProcessingException" , e);
				}
			}
			
			return users.size();
		} else {
			throw new UserDemoException("Get user failed. Response null.");
		}
	}

	private void saveUser(UserDto user) {
		log.info(user.toString());
		userRepository.save(new User(user));
	}
	
	public void postMessage(String message) {
        ListenableFuture<SendResult<String, String>> listenableFuture =
        		kafkaTemplateForString.send("simple", "", message);

        listenableFuture.addCallback(new ListenableFutureCallback<SendResult<String, String>>() {

            @Override
            public void onSuccess(SendResult<String, String> result) {
                log.info("message sent, partition={}, offset={}", result.getRecordMetadata().partition(),
                        result.getRecordMetadata().offset());
            }

            @Override
            public void onFailure(Throwable throwable) {
                log.warn("failed to send, message={}", message, throwable);
            }
        });

    }

}
