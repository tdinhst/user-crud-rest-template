package com.trangdtv.userdemo.controller;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.trangdtv.userdemo.entities.User;
import com.trangdtv.userdemo.exception.UserDemoException;
import com.trangdtv.userdemo.service.UserService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/users")
@Validated
public class UserController {
	@Autowired
	UserService userSerivce;
	
	@PostMapping
	public ResponseEntity<Object> create(@Valid @RequestBody @NotNull User user) throws UserDemoException{ 
		return ResponseEntity.ok(userSerivce.create(user));
		
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Object> update(@Valid @PathVariable("id") long id,@Valid  @RequestBody @NotNull User user) throws UserDemoException{
		user.setId(id);
		return ResponseEntity.ok(userSerivce.update(user));
		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Object> delete(@PathVariable("id") long id) throws UserDemoException{
		userSerivce.delete(id);
		return ResponseEntity.ok("Delete Sucessfully");
		
	}
	
	
	@GetMapping("/{id}")
	public ResponseEntity<Object> getById(@PathVariable("id") long id) throws UserDemoException{
		if(userSerivce.findById(id) != null) {
			return ResponseEntity.ok(userSerivce.findById(id));
		}
		return new ResponseEntity<>("User not found.",HttpStatus.NOT_FOUND);
				
	}
	
	@GetMapping
	public ResponseEntity<Object> findAll(@RequestParam(required = false) String name, @RequestParam int pageNumber, @RequestParam int numberOfPage){
		return ResponseEntity.ok(userSerivce.findAll(name, pageNumber, numberOfPage));
	}
	
	@GetMapping("/kafka")
	public ResponseEntity<Object> getUsersAndSendToKafka(){
		userSerivce.getUsersAndSendToKafka();
		return ResponseEntity.ok("Get data and send to Kafka succesfully.");
	}


}
