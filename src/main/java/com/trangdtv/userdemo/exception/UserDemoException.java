package com.trangdtv.userdemo.exception;

public class UserDemoException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  public UserDemoException(String message) {
      super(message);
  }

  public UserDemoException(String message, Throwable t) {
      super(message, t);
  }

}
