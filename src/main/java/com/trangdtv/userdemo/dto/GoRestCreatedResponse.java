package com.trangdtv.userdemo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GoRestCreatedResponse{
	
	private String meta;
	
	private int code;
	
	@JsonProperty("data")
	private UserDto user;

	public String getMeta() {
		return meta;
	}

	public void setMeta(String meta) {
		this.meta = meta;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public UserDto getUser() {
		return user;
	}

	public void setUser(UserDto data) {
		this.user = data;
	}

	@Override
	public String toString() {
		return "GoRestCreatedResponse [meta=" + meta + ", code=" + code + ", user=" + user + "]";
	}
	
	

}
