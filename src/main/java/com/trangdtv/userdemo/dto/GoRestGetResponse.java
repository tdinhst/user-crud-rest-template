package com.trangdtv.userdemo.dto;

import java.util.List;

public class GoRestGetResponse{
	
	private Meta meta;
	
	private Integer code;
	
	private List<UserDto> data;

	public Meta getMeta() {
		return meta;
	}

	public void setMeta(Meta meta) {
		this.meta = meta;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public List<UserDto> getData() {
		return data;
	}

	public void setData(List<UserDto> data) {
		this.data = data;
	}
	
	public class Meta{
		private GoRestPagination pagination;

		public GoRestPagination getPagination() {
			return pagination;
		}

		public void setPagination(GoRestPagination pagination) {
			this.pagination = pagination;
		}
		
		
	}

}
