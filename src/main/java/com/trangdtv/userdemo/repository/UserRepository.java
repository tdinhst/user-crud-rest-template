package com.trangdtv.userdemo.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.trangdtv.userdemo.entities.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{
	int countByEmail(String email);
	int countById(long id);
	List<User> findAllByName(String name, Pageable pageable);

}
