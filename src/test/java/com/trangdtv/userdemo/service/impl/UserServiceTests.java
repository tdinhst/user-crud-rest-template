package com.trangdtv.userdemo.service.impl;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.TopicPartition;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.test.context.TestPropertySource;
import org.springframework.util.concurrent.SettableListenableFuture;

import com.trangdtv.userdemo.dto.GoRestCreatedResponse;
import com.trangdtv.userdemo.dto.GoRestGetResponse;
import com.trangdtv.userdemo.dto.UserDto;
import com.trangdtv.userdemo.entities.StatusUser;
import com.trangdtv.userdemo.entities.User;
import com.trangdtv.userdemo.repository.UserRepository;
import com.trangdtv.userdemo.util.GorestClient;

/**
*
**/
@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
@TestPropertySource("/application.properties")
class UserServiceTests {
	
	@InjectMocks
	private UserServiceImpl userService;
	
	@Mock
	private GorestClient goRestClient;
	
	@Mock
	private UserRepository userRepository;
	
	@Mock
    @Qualifier(value = "kafkaTemplateForString")
	private KafkaTemplate<String, String> kafkaTemplateForString;

	@Test
	void createUser() {
		User user = this.initUser();
		when(goRestClient.createUser(any(User.class))).thenReturn(successCreateResponse(201));
		when(userRepository.countByEmail(anyString())).thenReturn(0);
		when(userRepository.save(any(User.class))).thenReturn(user);
		
		User userCreated = userService.create(user);
		assertEquals(userCreated.getId(), user.getId());
		
	}
	
	@Test
	void createUserEmailMoreThanZero() {
		User user = this.initUser();
		when(goRestClient.createUser(any(User.class))).thenReturn(successCreateResponse(201));
		when(userRepository.countByEmail(anyString())).thenReturn(1);
		try {

			userService.create(user);
			fail("Not throw exception");
		}catch(Exception e){
			assertNotNull(e);
		}
		
	}
	
	@Test
	void createUserFail() {
		User user = this.initUser();
		when(goRestClient.createUser(any(User.class))).thenReturn(failCreateResponse());
		try {

			userService.create(user);
			fail("Not throw exception");
		}catch(Exception e){
			assertNotNull(e);
		}
		
	}
	
	@Test
	void updateUser() {
		User user = this.initUser();
		Optional<User> userOptional = Optional.of(user);

		when(goRestClient.updateUser(any(User.class))).thenReturn(successCreateResponse(200));
		when(userRepository.countById(any(Long.class))).thenReturn(1);
		when(userRepository.findById(any(Long.class))).thenReturn(userOptional);
		when(userRepository.countByEmail(anyString())).thenReturn(0);
		when(userRepository.save(any(User.class))).thenReturn(user);
		
		User userCreated = userService.update(user);
		assertEquals(userCreated.getId(), user.getId());
		
	}
	
	@Test
	void updateUserFail() {
		User user = this.initUser();

		when(goRestClient.updateUser(any(User.class))).thenReturn(failCreateResponse());
		try {

			userService.update(user);
			fail("Not throw exception");
		}catch(Exception e){
			assertNotNull(e);
		}
		
	}
	
	@Test
	void updateUserNotFound() {
		User user = this.initUser();

		when(goRestClient.updateUser(any(User.class))).thenReturn(successCreateResponse(200));
		when(userRepository.countById(any(Long.class))).thenReturn(0);
		try {

			userService.update(user);
			fail("Not throw exception");
		}catch(Exception e){
			assertNotNull(e);
		}
		
	}
	
	@Test
	void updateUserEmailMoreThanZero() {
		User user = this.initUser();
		User user2 = this.initUser();
		user2.setEmail("a123@fafdg.com");
		Optional<User> userOptional = Optional.of(user2);
		when(goRestClient.updateUser(any(User.class))).thenReturn(successCreateResponse(200));
		when(userRepository.countById(any(Long.class))).thenReturn(1);
		when(userRepository.findById(any(Long.class))).thenReturn(userOptional);
		when(userRepository.countByEmail(anyString())).thenReturn(1);
		try {

			userService.update(user);
			fail("Not throw exception");
		}catch(Exception e){
			assertNotNull(e);
		}
		
	}
	
	@Test
	void deleteUser() {
		when(goRestClient.deleteUser(any(Long.class))).thenReturn(successCreateResponse(204));
		when(userRepository.countById(any(Long.class))).thenReturn(1);
		doNothing().when(userRepository).deleteById(any(Long.class));
		try {

			userService.delete(1);
		}catch(Exception e){
			System.out.println(e);
			fail("Throw exception");
		}
		
	}
	
	@Test
	void deleteUserNotFound() {
		when(goRestClient.deleteUser(any(Long.class))).thenReturn(successCreateResponse(204));
		when(userRepository.countById(any(Long.class))).thenReturn(0);
		try {

			userService.delete(1);
			fail("Not throw exception");
		}catch(Exception e){
			assertNotNull(e);
		}
		
	}
	
	@Test
	void deleteUserFail() {
		when(goRestClient.deleteUser(any(Long.class))).thenReturn(successCreateResponse(500));
		try {

			userService.delete(1);

			fail("Not throw exception");
		}catch(Exception e){
			assertNotNull(e);
		}
		
	}
	
	@Test
	void findById() {

		User user = this.initUser();
		Optional<User> userOptional = Optional.of(user);
		when(userRepository.findById(any(Long.class))).thenReturn(userOptional);
		User userGet = userService.findById(1);
		assertEquals(user.getId(), userGet.getId());
	}
	
	@Test
	void findByIdNull() {
		Optional<User> userOptional = Optional.empty();
		when(userRepository.findById(any(Long.class))).thenReturn(userOptional);
		User userGet = userService.findById(1);
		assertNull(userGet);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void findAll() {
		Page<User> users = Mockito.mock(Page.class);
		when(goRestClient.getUsers()).thenReturn(successGetResponse(200));
		when(userRepository.findAll(any(PageRequest.class))).thenReturn(users);
		userService.findAll("", 0, 10);
	}
	
	@Test
	void findAllWithName() {
		List<User> users = new ArrayList<>();
		when(goRestClient.getUsers()).thenReturn(successGetResponse(200));
		when(userRepository.findAllByName(anyString(),any(PageRequest.class))).thenReturn(users);
		userService.findAll("name", 0, 10);
	}
	
	@Test
	void getUsersAndSendToKafka() {
		User user = this.initUser();
		when(goRestClient.getUsers()).thenReturn(successGetResponse(200));
		when(userRepository.save(any(User.class))).thenReturn(user);
		when(kafkaTemplateForString.send(anyString(),anyString(),anyString())).thenReturn(this.kafkaSendSuccess());
		
		Integer usersNo = userService.getUsersAndSendToKafka();
		assertEquals(usersNo, this.successGetResponse(200).getData().size());
	}
	
	@Test
	void getUsersAndSendToKafkaResponseFail() {
		when(goRestClient.getUsers()).thenReturn(successGetResponse(500));
		try {
			userService.getUsersAndSendToKafka();
			fail("Not throw exception");
		}catch(Exception e){
			assertNotNull(e.getMessage());
		}
	}
	
	@Test
	void getUsersAndSendToKafkaResponseNull() {
		when(goRestClient.getUsers()).thenReturn(null);
		try {
			userService.getUsersAndSendToKafka();
			fail("Not throw exception");
		}catch(Exception e){
			assertNotNull(e.getMessage());
		}
	}
	
	User initUser() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(2021, 11, 20, 59, 59, 59);
		Date date = calendar.getTime();
		User user = new User();
		user.setId(1);
		user.setName("trangdtv");
		user.setEmail("trangdtv@fsoft.com.vn");
		user.setGender("Female");
		user.setStatus(StatusUser.Active);
		user.setCreateDate(date);
		user.setUpdateDate(date);
		
		return user;
	}
	
	GoRestGetResponse successGetResponse(Integer code) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(2021, 11, 20, 59, 59, 59);
		Date date = calendar.getTime();
		UserDto user = new UserDto();
		user.setId(1);
		user.setName("trangdtv");
		user.setEmail("trangdtv@fsoft.com.vn");
		user.setGender("Female");
		user.setStatus(StatusUser.Active.value());
		user.setCreateDate(date);
		user.setUpdateDate(date);
		List<UserDto> users = new ArrayList<>();
		users.add(user);
		GoRestGetResponse res = new GoRestGetResponse();
		res.setCode(code);
		res.setData(users);
		return res;
	}
	
	GoRestCreatedResponse successCreateResponse(int code) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(2021, 11, 20, 59, 59, 59);
		Date date = calendar.getTime();
		UserDto user = new UserDto();
		user.setId(1);
		user.setName("trangdtv");
		user.setEmail("trangdtv@fsoft.com.vn");
		user.setGender("Female");
		user.setStatus(StatusUser.Active.value());
		user.setCreateDate(date);
		user.setUpdateDate(date);
		GoRestCreatedResponse res = new GoRestCreatedResponse();
		res.setCode(code);
		res.setMeta("");
		res.setUser(user);
		return res;
	}
	
	GoRestCreatedResponse failCreateResponse() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(2021, 11, 20, 59, 59, 59);
		Date date = calendar.getTime();
		UserDto user = new UserDto();
		user.setId(1);
		user.setName("trangdtv");
		user.setEmail("trangdtv@fsoft.com.vn");
		user.setGender("Female");
		user.setStatus(StatusUser.Active.value());
		user.setCreateDate(date);
		user.setUpdateDate(date);
		GoRestCreatedResponse res = new GoRestCreatedResponse();
		res.setCode(422);
		res.setMeta("");
		return res;
	}
	
	SettableListenableFuture<SendResult<String, String>> kafkaSendSuccess(){
		SettableListenableFuture<SendResult<String, String>> future = new SettableListenableFuture<>();
		ProducerRecord<String,String> record = new ProducerRecord<String, String>("simple", "value");
		RecordMetadata recordMetadata = new RecordMetadata(new TopicPartition("simple", 1), 1, 1, 1, (long) 1, 1, 1);
		SendResult<String, String> sendResult = new SendResult<String, String>(record, recordMetadata);
		future.set(sendResult);
		return future;
	}
	
	SettableListenableFuture<SendResult<String, String>> kafkaSendFail(){
		SettableListenableFuture<SendResult<String, String>> future = new SettableListenableFuture<>();
		ProducerRecord<String,String> record = new ProducerRecord<String, String>("simple", "value");
		RecordMetadata recordMetadata = new RecordMetadata(new TopicPartition("simple", 1), 1, 1, 1, (long) 1, 1, 1);
		SendResult<String, String> sendResult = new SendResult<String, String>(record, recordMetadata);
		future.set(sendResult);
		future.setException(new Exception("Fail."));
		return future;
	}

}
